Source files for the [Datenschule Animation](http://www.cameralibre.cc/datenschule/).

Video licensed CC-BY Open Knowledge Foundation Deutschland e.V.

Designs & source files CC-BY Sam Muirhead


![overwhelmed](https://media.giphy.com/media/93ipu7e6t3CL39ulC5/giphy.gif)

![Datenschule hilft](https://media.giphy.com/media/3rivMOAgLbY0VTFGZD/giphy.gif)

![Karte](https://media.giphy.com/media/dCACK7tajtsflQXxYm/giphy.gif)

![Office-Magic](https://media.giphy.com/media/2vkMYreRnRs1nrxJfa/giphy.gif)

![Nerds](https://media.giphy.com/media/w6ohWeYO8nLN2v8b94/giphy.gif)

